/*
 * Copyright (c) 2016 Zibin Zheng <znbin@qq.com>
 * All rights reserved
 */

#include "multi_timer.h"

//timer handle list head.
static struct Timer* head_handle = NULL;

//Timer ticks
static uint32_t _timer_ticks = 0;

/**
  * @brief  Initializes the timer struct handle.
  * @param  handle: the timer handle strcut.
  * @param  timeout_cb: timeout callback.
  * @param  repeat: repeat interval time.
  * @retval None
  */
void timer_init(struct Timer* handle, void (*timeout_cb)(), uint32_t timeout, uint32_t repeat)
{
    // memset(handle, sizeof(struct Timer), 0);
    handle->timeout_cb = timeout_cb;
    handle->timeout    = _timer_ticks + timeout;
    handle->repeat     = repeat;
}

/**
  * @brief  Start the timer work, add the handle into work list.
  * @param  btn: target handle strcut.
  * @retval 0: succeed. -1: already exist.
  */
int timer_start(struct Timer* handle)
{
    struct Timer* target = head_handle;
    while(target)
    {
        if(target == handle)
            return -1;  //already exist.
        target = target->next;
    }
    handle->next = head_handle;
    head_handle  = handle;
    return 0;
}

/**
  * @brief  Stop the timer work, remove the handle off work list.
  * @param  handle: target handle strcut.
  * @retval None
  */
void timer_stop(struct Timer* handle)
{
    struct Timer** curr;
    for(curr = &head_handle; *curr;)
    {
        struct Timer* entry = *curr;
        if(entry == handle)
        {
            *curr = entry->next;
            //			free(entry);
        }
        else
            curr = &entry->next;
    }
}

/**
  * @brief  main loop.
  * @param  None.

  * @retval None
  */
void timer_loop()
{
    struct Timer* target;
    for(target = head_handle; target; target = target->next)
    {
        if(_timer_ticks >= target->timeout)
        {
            if(target->repeat == 0)
            {
                timer_stop(target);
            }
            else
            {
                target->timeout = _timer_ticks + target->repeat;
            }
            target->timeout_cb();
        }
    }
}

/**
  * @brief  background ticks, timer repeat invoking interval 1ms.
  * @param  None.
  * @retval None.
  */
void timer_ticks()
{
    _timer_ticks++;
}

/**
    从结构看，这是个比较好的构件。
    问题1：定义一个实体定时器，如果程序需要多个定时器，而且有很多都是一次性的，那么要考虑使用
    动态分配，那么要考虑回收定时器的地方和创建实体定时器的地方。

    问题2：如果一个回调函数需要执行的时间比较长，那么势必导致下一个定时器的回调会延迟。
    这是必然发生的。那么定时回调函数只能实现比较执行时间短的功能。
    或者在考虑滴答的周期变的大一些。

    timer_loop这个核心调度是否考虑放到滴答中断里（产生定时周期的中断），这样可以及时的发现
    定时器时间到了。

    比较好的地方，就是可以使用大量的定时器，且有首次延迟，周期或单次执行。

    如果有定时器需要周期性，但是又有次数要求，那么则需要自定义
    考虑建立一个结构体，包含此定时器结构体，另外增加一个变量做次数，
    然后再建立初始化函数。这就有点像C++里的继承了。
    
*/